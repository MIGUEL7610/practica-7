package mx.unitec.moviles.practica7.widget

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mx.unitec.moviles.practica7.R
import mx.unitec.moviles.practica7.model.Contact
import java.util.zip.Inflater

class ContactListAdapter internal constructor(context: Context) :
RecyclerView.Adapter<ContactListAdapter.ContacViedHolder>() {

    private val inflanter: LayoutInflater = LayoutInflater.from(context)
    private var contacts = listOf<Contact>()


    inner class ContacViedHolder(itemView: View): RecyclerView.ViewHolder(itemView){
  val contactNameItemView: TextView = itemView.findViewById(R.id.ttvName)
        val contactPhoneItemView: TextView = itemView.findViewById(R.id.ttvPhone)
        val contactEmailItemView: TextView = itemView.findViewById(R.id.ttvEmail)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContacViedHolder {
       val itemView = inflanter.inflate(R.layout.recyclerview_item,parent,false)
       return ContacViedHolder(itemView)
    }

    override fun getItemCount() = contacts.size

    override fun onBindViewHolder(holder: ContacViedHolder, position: Int) {
        val current = contacts[position]

        holder.contactNameItemView.text = current.name
        holder.contactPhoneItemView.text = current.phone
        holder.contactEmailItemView.text = current.email
    }


    internal fun setContacts(contacts: List<Contact>){
        this.contacts = contacts
        notifyDataSetChanged()
    }
    internal fun getContactAt(position: Int): Contact{
        return contacts[position]
    }
}